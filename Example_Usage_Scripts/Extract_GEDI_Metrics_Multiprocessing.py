#!/usr/bin/env/python3
'''
Description: A script demonstrating how to extract GEDI Level 2B metrics
             for several HDF-5 files using multi-core processing.
Authors: Suvarna Punalekar & Osian Roberts (Aberystwyth University).
'''
from GEDILib import apply_batch_processor

inDir = './Path_to_GEDI_Data'
outDir = './GEDI_Metrics'

# define coordinates of ROI:
xmin, xmax = -5.5, -2.5
ymin, ymax = 51.0, 53.5

apply_batch_processor(inDir, outDir, xmin, xmax, ymin, ymax, ncores=4,
                      searchString='GEDI02_B*.h5', vector=True, csv=True,
                      sep=',', header=True, index=False, hdf=True)

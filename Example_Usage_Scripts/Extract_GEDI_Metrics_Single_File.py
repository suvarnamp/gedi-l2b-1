#!/usr/bin/env/python3
'''
Description: A script demonstrating how to extract GEDI Level 2B metrics for a
             single HDF-5 file.
Authors: Suvarna Punalekar & Osian Roberts (Aberystwyth University).
'''
import GEDILib

fname = 'GEDI02_B_2019191234645_O03263_T02767_02_001_01.h5'

# define coordinates of ROI:
xmin, xmax = -60.0, -59.5
ymin, ymax = -16.0, -15.5

# read GEDI Level 2B data in ROI:
ds = GEDILib.read_l2b(fname, xmin, xmax, ymin, ymax)

# print the data for sanity check:
#print(ds.metadata)
#print(ds.data)

# export metrics to CSV:
#ds.export_to_CSV('GEDI_L2B_Metrics.csv', sep=',', header=True, index=False)

# export metrics to vector:
ds.export_to_vector('GEDI_L2B_Metrics.gpkg')
del ds

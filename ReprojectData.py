#!/usr/bin/env/python3
'''
Description: A script to reproject GEDI L2B data at high latitudes when
             converting from WGS84.
Authors: Osian Roberts (Aberystwyth University).
'''
import argparse
import os
from osgeo import ogr, osr


def ReprojectData(infile, outfile, outformat='GPKG', insrs=4326, outsrs=32630,
                  laserDiameter=25.0):
    '''
    A function to reproject GEDI L2B data.

    Required parameters:
    infile = String defining the input file.
    outfile = String defining the output file.

    Optional parameters:
    outformat = Output vector format. Default = 'GPKG'.
    insrs = Output EPSG code. Default = 4326.
    outsrs = Output EPSG code. Default = 32630.
    laserDiameter = Laser footprint diameter in metres. Default = 25.
    '''
    # check input parameters:
    if not isinstance(infile, str):
        raise SystemExit('Error: input file name must be a string.')
    if not isinstance(outfile, str):
        raise SystemExit('Error: output file name must be a string.')
    if not isinstance(outformat, str):
        raise SystemExit('Error: outformat must be a string.')
    if not isinstance(insrs, int):
        raise SystemExit('Error: input EPSG code must be an integer.')
    if not isinstance(outsrs, int):
        raise SystemExit('Error: output EPSG code must be an integer.')
    if not isinstance(laserDiameter, float) and not insinstance(laserDiameter, int):
        raise SystemExit('Error: laser footprint diameter must be numeric.')
    
    print('Reprojecting ' + os.path.basename(infile) + '...')

    # try opening the input file:
    try:
        invector = ogr.Open(infile, 0)
        inlayer = invector.GetLayer()
    except Exception:
        raise SystemExit('Error: unable to open input vector.')

    # get output driver:
    try:
        driver = ogr.GetDriverByName(outformat)
    except Exception:
        raise SystemExit('Error: output vector format not recognised.')

    # delete existing vector if present:
    if os.path.exists(outfile):
        driver.DeleteDataSource(outfile)

    # create vector dataset:
    outvector = driver.CreateDataSource(outfile)

    # define coordinate systems:
    sourcesrs = osr.SpatialReference()
    sourcesrs.ImportFromEPSG(insrs)
    targetsrs = osr.SpatialReference()
    targetsrs.ImportFromEPSG(outsrs)

    transform = osr.CoordinateTransformation(sourcesrs, targetsrs)

    # define output vector layer:
    layername = os.path.basename(outfile).split('.')[0]
    outlayer = outvector.CreateLayer(layername, targetsrs, geom_type=ogr.wkbMultiPolygon)
    del layername

    # copy fields to output vector:
    fieldnames = [field.name for field in inlayer.schema]
    for field in inlayer.schema:
        f = ogr.FieldDefn(field.name, field.type)
        f.SetWidth(field.width)
        f.SetPrecision(field.precision)
        outlayer.CreateField(f)
        del f
    
    # iterate over each feature in input layer:
    for feature in inlayer:
        # get centroid of laser footprint:
        centroid = feature.GetGeometryRef().Centroid()
        # reproject centroid:
        centroid.Transform(transform)
        # buffer centroid:
        poly = centroid.Buffer(laserDiameter / 2)
        del centroid

        # create feature in output vector layer:
        outfeature = ogr.Feature(outlayer.GetLayerDefn())
        outfeature.SetGeometry(poly)
        del poly

        # populate fields:
        for name in fieldnames:
            outfeature.SetField(name, feature.GetField(name))

        # write feature to vector:
        outlayer.CreateFeature(outfeature)
        outlayer.SyncToDisk()
        del feature, outfeature
    del transform, sourcesrs, targetsrs

    # close datasets:
    del invector, outvector, inlayer, outlayer, fieldnames
    print('Done.')


######################################################

parser = argparse.ArgumentParser(prog='Reproject Data',
description='A script to reproject GEDI L2B data at high latitudes when converting from WGS84.')

required = parser.add_argument_group('Required Arguments')
required.add_argument('-input', metavar='', type=str, help='Input file.')
required.add_argument('-output', metavar='', type=str, help='Output file.')

optional = parser.add_argument_group('Optional Arguments')
optional.add_argument('-of', metavar='', type=str, default='GPKG',
                      help='Output vector format. Default = "GPKG".')
optional.add_argument('-s_srs', metavar='', type=int, default='4326',
                      help='Source EPSG code. Default = 4326.')
optional.add_argument('-t_srs', metavar='', type=int, default='32630',
                      help='Target EPSG code. Default = 32630.')
optional.add_argument('-d', metavar='', type=float, default='25.0',
                      help='Target laser footprint diameter. Default = 25.0 m.')
args = parser.parse_args()

if not args.input:
	parser.print_help()
	raise SystemExit('\nError: Please specify an input file.')
elif not os.path.exists(args.input):
	parser.print_help()
	raise SystemExit('\nError: input file not found. Please check the file path.')
elif not args.output:
	parser.print_help()
	raise SystemExit('\nError: Please specify an output file.')
else:
    ReprojectData(args.input, args.output, args.of, args.s_srs, args.t_srs, args.d)
